class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def index
    @posts = Post.paginate(:page => params[:page], :per_page => 2)
  end

  def show
    #@post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to @post, success: 'Article successfully created'
    else
      flash.now[:danger] = 'Статья не создана'
      render :new
    end
  end

    def edit
      #@post = Post.find(params[:id])
    end


  def update
    #@post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      redirect_to @post, success: 'Статья успешно обновлена'
    else
      flash.now[:danger] = 'Статья не обновлена'
      render :edit
    end
  end

  def destroy
    #@post = Post.find(params[:id])
    @post.destroy
    redirect_to posts_path, success: 'Article successfully deleted'
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :summary, :body, :all_tags, :category_id, :image)
  end
end